
from you.utils.syntax import *


def test_is_valid_verb():
    assert is_valid_verb('foo')
    assert is_valid_verb('FOO')
    assert is_valid_verb('2018')
    assert is_valid_verb('foo_bar')
    assert is_valid_verb('foo-bar')
    assert is_valid_verb('foo-bar_2018')
    assert not is_valid_verb('foo-bar 2018')
    assert not is_valid_verb([])
    assert not is_valid_verb(None)

def test_parse_port():
    assert parse_port('80') == 80
    assert parse_port('8080') == 8080
    assert parse_port('0') == 0
    assert parse_port('-1') == None
    assert parse_port('65536') == None

def test_parse_hostport():
    assert parse_hostport('[2001:db8::1]:8080') == ('2001:db8::1', 8080)
    assert parse_hostport('[2001:db8::1]') == ('2001:db8::1', 0)
    assert parse_hostport('127.0.0.1:8080') == ('127.0.0.1', 8080)
    assert parse_hostport('127.0.0.1') == ('127.0.0.1', 0)
    assert parse_hostport('HostName:8080') == ('HostName', 8080)
    assert parse_hostport('www.example.com:8080') == ('www.example.com', 8080)
    assert parse_hostport('') == ('localhost', 0)
    assert parse_hostport('127.0.0.1:65536') == None
    assert parse_hostport(':') == None
