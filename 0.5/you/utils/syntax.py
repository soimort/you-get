
import ipaddress  # python >= 3.3
import re


def is_valid_verb(s):
    """Check if a string is a valid YOU-verb.

    A YOU-verb is a nonempty sequence of alphanumeric ASCII characters.
    (including '_' and '-')
    """
    if type(s) is not str:
        return False

    return re.fullmatch(r'[\w-]+', s) is not None


def parse_port(s):
    """Parse a string representing a 'PORT'.
    Return an int if succeeded; otherwise return a NoneType.
    """
    if type(s) is not str:
        return None

    try:
        portno = int(s)
    except ValueError:
        # invalid literal for int() with base 10: '[...]'
        return None
    if 0 <= portno < 2 ** 16:  # maximum port number: 65535
        return portno
    return None


def parse_hostport(s, default_host='localhost', default_port=0):
    """Parse a string representing a 'HOST:PORT' pair.
    Return a str * int, if succeeded; otherwise return a NoneType.
    """
    if type(s) is not str:
        return None

    iplike = re.fullmatch(r'\[(.+)\](:(\d+))?', s) or \
        re.fullmatch(r'(\d+\.\d+\.\d+\.\d+)(:(\d+))?', s)
    if iplike is not None:
        ipaddr = iplike.group(1)
        try:
            ipaddress.ip_address(ipaddr)
        except ValueError:
            # '[...]' does not appear to be an IPv4 or IPv6 address
            return None

        if iplike.group(2) is None:
            portno = default_port
        else:
            portno = parse_port(iplike.group(3))
        if portno is not None:
            return ipaddr, portno
        return None

    namelike = re.fullmatch(r'([^:]*)(:(\d+))?', s)
    if namelike is not None:
        name = namelike.group(1) or default_host

        if namelike.group(2) is None:
            portno = default_port
        else:
            portno = parse_port(namelike.group(3))
        if portno is not None:
            return name, portno
        return None

    return None


def show_hostport(host, port):
    return '{}:{}'.format(host, port)
